package io.github.allanchain.autowifi

import androidx.compose.material.Switch
import androidx.compose.runtime.Composable

@Composable
fun AutoWiFiApp(
    appState: AutoWiFiState = rememberAutoWifiState()
) {
    Switch(checked = appState.isConnected, onCheckedChange = { appState.connectWifi() })

}