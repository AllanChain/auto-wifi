package io.github.allanchain.autowifi

import android.content.Context
import android.net.*
import android.net.wifi.WifiNetworkSpecifier
import android.os.PatternMatcher
import android.widget.Toast
import androidx.compose.runtime.*
import androidx.compose.ui.platform.LocalContext
import android.net.NetworkCapabilities

import android.net.NetworkRequest

import android.R.string.no
import android.net.wifi.WifiInfo


@Composable
fun rememberAutoWifiState(
    context: Context = LocalContext.current
) = remember(context) {
    AutoWiFiState(context)
}

class AutoWiFiState(private val context: Context) {
    var isConnected by mutableStateOf(false)

    fun refreshConnected() {
        val request = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .build()
        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val networkCallback = object : ConnectivityManager.NetworkCallback() {
            override fun onAvailable(network: Network) {}

            override fun onCapabilitiesChanged(
                network: Network,
                networkCapabilities: NetworkCapabilities
            ) {
                val wifiInfo = networkCapabilities.transportInfo as WifiInfo
                Toast.makeText(context, wifiInfo.ssid, Toast.LENGTH_SHORT).show()
                isConnected = wifiInfo.ssid === "THU Insecure"
            }
        }
        connectivityManager.requestNetwork(request, networkCallback)
    }

    fun connectWifi() {
        val specifier = WifiNetworkSpecifier.Builder()
            .setSsidPattern(PatternMatcher("THU Insecure", PatternMatcher.PATTERN_LITERAL))
//            .setBssidPattern(MacAddress.fromString("10:03:23:00:00:00"), MacAddress.fromString("ff:ff:ff:00:00:00"))
            .build()

        val request = NetworkRequest.Builder()
            .addTransportType(NetworkCapabilities.TRANSPORT_WIFI)
            .removeCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
            .setNetworkSpecifier(specifier)
            .build()

        val connectivityManager =
            context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        val networkCallback = object : ConnectivityManager.NetworkCallback() {

            override fun onAvailable(network: Network) {
                Toast.makeText(context, "Success", Toast.LENGTH_SHORT).show()
            }

            override fun onUnavailable() {
                Toast.makeText(context, "Failure", Toast.LENGTH_SHORT).show()
            }

        }
        connectivityManager.requestNetwork(request, networkCallback)

// Release the request when done.
        connectivityManager.unregisterNetworkCallback(networkCallback)

    }
}